FROM cern/slc6-base:latest

RUN yum install -y \
      zip \
      tar \
      wget \
      which \
      libXpm-devel \
      libXft-devel \
      libXext-devel \
      gcc-c++.x86_64 \
      libX11-devel \
      glibc-devel.i686 \
      glibc-devel \
      python-devel \
      cmake && \
    yum clean all

WORKDIR /code
RUN wget https://github.com/root-project/root/archive/v5-34-24.tar.gz && \
    tar -xzvf v5-34-24.tar.gz && \
    mkdir -p /code/build && \
    cd /code/build && \
    PYTHON_MINOR_VERSION=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))') && \
    cmake \
      -Dall=OFF \
      -Dfortran=OFF \
      -Droofit=ON \
      -Droostats=ON \
      -Dhistfactory=ON \
      -Dminuit2=ON \
      -Dpython=ON \
      -DPYTHON_EXECUTABLE=$(command -v python) \
      -DPYTHON_LIBRARIES=/usr/lib/python${PYTHON_MINOR_VERSION} \
      -DPYTHON_INCLUDE_DIRS=/usr/include/python${PYTHON_MINOR_VERSION} \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      ../root-5-34-24 && \
    cmake -L . && \
    cmake --build . -- -j$(($(nproc)-2)) && \
    cmake --build . --target install && \
    cd / && \
    rm -rf /code

ENV DISPLAY=localhost:0.0
ENV PYTHONPATH=/usr/local/lib:$PYTHONPATH
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
ENV ROOTSYS=/usr/local
WORKDIR /root
